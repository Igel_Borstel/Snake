#include "../include/Snake.hpp"

namespace snake
{
	Snake::Snake(std::vector<std::shared_ptr<sf::Texture>>& ts, std::vector<sf::Vector2i> sb, int fs, bool s)
		: textures(ts), fieldsize(fs)
	{
		reset(sb, s);
	}
	Snake::Snake(std::vector<std::shared_ptr<sf::Texture>>& ts, int fs)
		: textures(ts), fieldsize(fs)
	{
	}
	void Snake::move(Direction direction)
	{
		oldSnakeHeadPosition = snakeBody.at(0);
		oldSnakeTailPosition = snakeBody.at(snakeBody.size() - 1);
		switch(direction)
		{
			case Direction::LEFT:
				--snakeBody.at(0).x;
				break;
			case Direction::RIGHT:
				++snakeBody.at(0).x;
				break;
			case Direction::FORWARD:
				--snakeBody.at(0).y;
				break;
			case Direction::BACKWARD:
				++snakeBody.at(0).y;
				break;
		}
		for(int i = snakeBody.size() - 1; i > 1; --i)
		{
			snakeBody.at(i).x = snakeBody.at(i - 1).x;
			snakeBody.at(i).y = snakeBody.at(i - 1).y;
		}
		//setze letzen Schlangenkörper auf alte Schlangenposition
		snakeBody.at(1).x = oldSnakeHeadPosition.x;
		snakeBody.at(1).y = oldSnakeHeadPosition.y;

		//update sprites
		int j = 0;
		if(sprites.size() == snakeBody.size())
		{
			for(std::size_t i = 0; i < sprites.size(); ++i)
			{
				if(0 == i)
				{
					switch(direction)
					{
						case Direction::LEFT:
							sprites.at(i).setRotation(0.0f);
							break;
						case Direction::RIGHT:
							sprites.at(i).setRotation(180.0f);
							break;
						case Direction::FORWARD:
							sprites.at(i).setRotation(90.0f);
							break;
						case Direction::BACKWARD:
							sprites.at(i).setRotation(270.0f);
							break;
					}
					sprites.at(i).setPosition((float) snakeBody.at(j).x * fieldsize + fieldsize / 2,
											  (float) snakeBody.at(j).y * fieldsize + fieldsize / 2);
					++j;
				}
				else
				{
					sprites.at(i).setPosition((float) snakeBody.at(j).x * fieldsize + fieldsize / 2,
											  (float) snakeBody.at(j).y * fieldsize + fieldsize / 2);
					++j;
				}
			}
		}
	}
	bool Snake::collides(Snake& snake)
	{
		for(std::size_t i = 1; i < snake.snakeBody.size(); ++i)
		{
			if(snakeBody.at(0) == snake.snakeBody.at(i))
			{
				return true;
			}
		}
		return false;
	}
	bool Snake::collides(int width, int hight)
	{
		if((snakeBody.at(0).x < 0 || snakeBody.at(0).x >(width - 1)) || //x-Achse (vertikal)
		   (snakeBody.at(0).y < 0 || snakeBody.at(0).y >(hight - 1))) //y-Achse (horizontal)
		{
			return true;
		}
		return false;
	}
	bool Snake::collides(sf::Vector2i pos, bool completeSnake)
	{
		if(completeSnake)
		{
			for(sf::Vector2i temp : snakeBody)
			{
				if(pos == temp)
				{
					return true;
				}
			}
			return false;
		}
		else
			return (pos == snakeBody.at(0))? true : false;
	}
	void Snake::grow(bool secondSnake)
	{
		snakeBody.push_back(oldSnakeTailPosition);

		//TODO: Dynamische Schlangentextureposition
		//weiteeren Schlangenkörper bei den Textures und Sprites hinzufügen
		if(!secondSnake)
			sprites.push_back(sf::Sprite(*textures.at(12)));
		else
			sprites.push_back(sf::Sprite(*textures.at(14)));
		sprites.at(sprites.size() - 1).setOrigin((float) textures.at(0)->getSize().x / 2,
												 (float) textures.at(0)->getSize().y / 2);

		//Skaliere wenn Feld größer als Texture ist
		if(textures.at(0)->getSize().x != fieldsize)
		{
			sprites.at(sprites.size() - 1).setScale((float) fieldsize / textures.at(0)->getSize().x,
													(float) fieldsize / textures.at(0)->getSize().y);
		}
		sprites.at(sprites.size() - 1).setPosition((float) snakeBody.at(sprites.size() - 1).x * fieldsize + fieldsize / 2,
												   (float) snakeBody.at(sprites.size() - 1).y * fieldsize + fieldsize / 2);
	}
	void Snake::reset(std::vector<sf::Vector2i> _pos, bool secondsnake)
	{
		sprites.clear();
		snakeBody.clear();
		bool head = false; //Kopf generiert?
		for(sf::Vector2i pos : _pos)
		{
			snakeBody.push_back(pos);
			if(!head)
				if(!secondsnake)
				{
					sprites.push_back(sf::Sprite(*textures.at(9)));
					head = true;
				}
				else
				{
					sprites.push_back(sf::Sprite(*textures.at(11)));
					head = true;
				}
			else
				if(!secondsnake)
					sprites.push_back(sf::Sprite(*textures.at(12)));
				else
					sprites.push_back(sf::Sprite(*textures.at(14)));
			sprites.at(sprites.size() - 1).setOrigin((float) textures.at(0)->getSize().x / 2,
													 (float) textures.at(0)->getSize().y / 2);
			if(textures.at(0)->getSize().x != fieldsize)
			{
				sprites.at(sprites.size() - 1).setScale((float) fieldsize / textures.at(0)->getSize().x,
														(float) fieldsize / textures.at(0)->getSize().y);
			}
		}
		int j = 0;
		if(sprites.size() == snakeBody.size())
		{
			for(std::size_t i = 0; i < sprites.size(); ++i)
			{
				sprites.at(i).setPosition((float) snakeBody.at(j).x * fieldsize + fieldsize / 2,
										  (float) snakeBody.at(j).y * fieldsize + fieldsize / 2);
				++j;
			}
		}
	}
	void Snake::draw(sf::RenderWindow& rw)
	{
		for(sf::Sprite sprite : sprites)
		{
			rw.draw(sprite);
		}
	}
}