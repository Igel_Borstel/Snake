#define _CRT_SECURE_NO_WARNINGS
#include "../include/Game.hpp"
#include <cstdlib>
#include <Logger.hpp>

using std::to_string;
using tinyxml2::XMLElement;

namespace snake
{
	void Game::onOptions()
	{
	}
	void Game::onCredits()
	{
		sf::Event event;
		sf::Time newTime;
		while(gameWindow.pollEvent(event))
		{
			switch(event.type)
			{
				case sf::Event::Closed:
					gameWindow.close();
					break;
				case sf::Event::MouseButtonPressed:
				case sf::Event::MouseMoved:
					newTime = gameClock.getElapsedTime();
					creditsToMainButton->update(false);
					elapsedTime = newTime;
					break;
				case sf::Event::MouseButtonReleased:
					newTime = gameClock.getElapsedTime();
					creditsToMainButton->update(true);
					elapsedTime = newTime;
					break;
			}
		}
		gameWindow.clear(sf::Color::Black);
		gameWindow.draw(backgroundSprite);
		creditsToMainButton->draw();
		gameWindow.draw(creditSprite);
		gameWindow.display();
	}
	void Game::onWelcome()
	{
		sf::Event event;
		while(gameWindow.pollEvent(event))
		{
			switch(event.type)
			{
				case sf::Event::Closed:
					gameWindow.close();
					break;
				case sf::Event::MouseButtonPressed:
				case sf::Event::MouseMoved:
					updateButtons(mainMenuButtons);
					break;
				case sf::Event::MouseButtonReleased:
					updateButtons(mainMenuButtons, true);
					break;
			}
		}
		renderWelcome();
	}
	void Game::renderWelcome()
	{
		gameWindow.clear(sf::Color::Black);
		gameWindow.draw(backgroundSprite);
		for(std::shared_ptr<Button> button : mainMenuButtons)
		{
			button->draw();
		}
		gameWindow.display();
	}
	void Game::onPausing()
	{
		sf::Event event;
		while(gameWindow.pollEvent(event))
		{
			switch(event.type)
			{
				case sf::Event::Closed:
					gameWindow.close();
					break;
				case sf::Event::MouseButtonPressed:
				case sf::Event::MouseMoved:
					updateButtons(pausingButtons);
					break;
				case sf::Event::MouseButtonReleased:
					updateButtons(pausingButtons, true);
					break;
			}
		}
		renderPausing();
	}
	void Game::renderPausing()
	{
		gameWindow.clear(sf::Color::Black);
		gameWindow.draw(backgroundSprite);
		for(std::shared_ptr<Button> button : pausingButtons)
		{
			button->draw();
		}
		gameWindow.display();
	}
	void Game::onPlaying()
	{
		sf::Event event;
		while(gameWindow.pollEvent(event))
		{
			switch(event.type)
			{
				case sf::Event::LostFocus:
					gamestate = GameState::PAUSING;
					break;
				case sf::Event::Closed:
					gameWindow.close();
					break;
				case sf::Event::KeyPressed:
					if(singleplayer)
						switch(event.key.code)
						{
							case sf::Keyboard::Escape:
								gamestate = GameState::PAUSING;
								break;
							case sf::Keyboard::A:
							case sf::Keyboard::Left:
								if(firstPlayerDirection != Direction::LEFT)
								{
									if(firstPlayerDirection != Direction::RIGHT)
									{
										firstPlayerDirection = Direction::LEFT;
									}
								}
								break;
							case sf::Keyboard::D:
							case sf::Keyboard::Right:
								if(firstPlayerDirection != Direction::RIGHT)
								{
									if(firstPlayerDirection != Direction::LEFT)
									{
										firstPlayerDirection = Direction::RIGHT;
									}
								}
								break;
							case sf::Keyboard::S:
							case sf::Keyboard::Down:
								if(firstPlayerDirection != Direction::BACKWARD)
								{
									if(firstPlayerDirection != Direction::FORWARD)
									{
										firstPlayerDirection = Direction::BACKWARD;
									}
								}
								break;
							case sf::Keyboard::W:
							case sf::Keyboard::Up:
								if(firstPlayerDirection != Direction::FORWARD)
								{
									if(firstPlayerDirection != Direction::BACKWARD)
									{
										firstPlayerDirection = Direction::FORWARD;
									}
								}
								break;
						}
					else
						switch(event.key.code)
						{
							case sf::Keyboard::Escape:
								gamestate = GameState::PAUSING;
								break;
							case sf::Keyboard::A:
								if(secondPlayerDirection != Direction::LEFT)
								{
									if(secondPlayerDirection != Direction::RIGHT)
									{
										secondPlayerDirection = Direction::LEFT;
									}
								}
								break;
							case sf::Keyboard::Left:
								if(firstPlayerDirection != Direction::LEFT)
								{
									if(firstPlayerDirection != Direction::RIGHT)
									{
										firstPlayerDirection = Direction::LEFT;
									}
								}
								break;
							case sf::Keyboard::D:
								if(secondPlayerDirection != Direction::RIGHT)
								{
									if(secondPlayerDirection != Direction::LEFT)
									{
										secondPlayerDirection = Direction::RIGHT;
									}
								}
								break;
							case sf::Keyboard::Right:
								if(firstPlayerDirection != Direction::RIGHT)
								{
									if(firstPlayerDirection != Direction::LEFT)
									{
										firstPlayerDirection = Direction::RIGHT;
									}
								}
								break;
							case sf::Keyboard::S:
								if(secondPlayerDirection != Direction::BACKWARD)
								{
									if(secondPlayerDirection != Direction::FORWARD)
									{
										secondPlayerDirection = Direction::BACKWARD;
									}
								}
								break;
							case sf::Keyboard::Down:
								if(firstPlayerDirection != Direction::BACKWARD)
								{
									if(firstPlayerDirection != Direction::FORWARD)
									{
										firstPlayerDirection = Direction::BACKWARD;
									}
								}
								break;
							case sf::Keyboard::W:
								if(secondPlayerDirection != Direction::FORWARD)
								{
									if(secondPlayerDirection != Direction::BACKWARD)
									{
										secondPlayerDirection = Direction::FORWARD;
									}
								}
								break;
							case sf::Keyboard::Up:
								if(firstPlayerDirection != Direction::FORWARD)
								{
									if(firstPlayerDirection != Direction::BACKWARD)
									{
										firstPlayerDirection = Direction::FORWARD;
									}
								}
								break;
						}
					break;
			}
		}
		updateGame();
		renderGame();
}
	void Game::updateGame()
	{
		sf::Time newTime = gameClock.getElapsedTime();
		if(newTime.asMilliseconds() - elapsedTime.asMilliseconds() >= tickTime.asMilliseconds())
		{
			elapsedTime = newTime;
			if(singleplayer)
			{
				firstSnake.move(firstPlayerDirection);

				//Pr�fe ob Schlange auf Wand getroffen ist:
				if(firstSnake.collides(width, hight))
				{
					gamestate = GameState::GAMEOVER;
				}

				if(firstSnake.collides(firstSnake))
				{
					gamestate = GameState::GAMEOVER;
				}

				//Verschiebe den Schlangenk�rper
				//�berpr�fe ob die Schlange auf eine Frucht getroffen ist
				if(firstSnake.collides(fruitPosition))
				{
					firstSnake.grow(false);
					generateNewFruit();
				}
			}
			else
			{
				firstSnake.move(firstPlayerDirection);
				secondSnake.move(secondPlayerDirection);

				//Pr�fe ob Schlange auf Wand getroffen ist:
				if(firstSnake.collides(width, hight) || secondSnake.collides(width, hight))
				{
					gamestate = GameState::GAMEOVER;
				}

				if(firstSnake.collides(firstSnake) || secondSnake.collides(secondSnake))
				{
					gamestate = GameState::GAMEOVER;
				}

				if(firstSnake.collides(secondSnake) || secondSnake.collides(firstSnake))
				{
					gamestate = GameState::GAMEOVER;
				}

				//Verschiebe den Schlangenk�rper
				//�berpr�fe ob die Schlange auf eine Frucht getroffen ist
				if(firstSnake.collides(fruitPosition))
				{
					firstSnake.grow(false);
					generateNewFruit();
				}
				else if(secondSnake.collides(fruitPosition))
				{
					secondSnake.grow(true);
					generateNewFruit();
				}
			}
		}
	}
	void Game::renderGame()
	{
		gameWindow.clear(sf::Color::Black);
		gameWindow.draw(backgroundSprite);
		gameWindow.draw(fruitSprite);
		firstSnake.draw(gameWindow);
		if(!singleplayer)
		{
			secondSnake.draw(gameWindow);
		}
		gameWindow.display();
	}
	void Game::generateNewFruit()
	{
		sl::Logger::writeToLog("Generate new Fruit!");
		bool valid = true;
		do
		{
			valid = true;
			fruitPosition.x = rand() % width;
			fruitPosition.y = rand() % hight;

			valid = (!firstSnake.collides(fruitPosition, true)) || (!secondSnake.collides(fruitPosition, true));
		} while(!valid);
		sl::Logger::writeToLog("Fruitposition is valid! Select right texture!");

		int randomeFruit = rand();
		if(randomeFruit == 39275)
		{
			textureAtlasFruitIndex = 2;
		}
		else if(randomeFruit % 10 == 0)
		{
			textureAtlasFruitIndex = 7;
		}
		else if(randomeFruit % 9 == 0)
		{
			textureAtlasFruitIndex = 6;
		}
		else if(randomeFruit % 8 == 0)
		{
			textureAtlasFruitIndex = 5;
		}
		else if(randomeFruit % 7 == 0)
		{
			textureAtlasFruitIndex = 4;
		}
		else if(randomeFruit % 5 == 0)
		{
			textureAtlasFruitIndex = 3;
		}
		else if(randomeFruit % 3 == 0)
		{
			textureAtlasFruitIndex = 1;
		}
		else if(randomeFruit % 2 == 0)
		{
			textureAtlasFruitIndex = 0;
		}
		else
		{
			textureAtlasFruitIndex = 7;
		}
		if(textureAtlasFruitIndex == christmasHatIndex && christmasmode)
		{
			christmasHatGenerated = true;
		}
		fruitSprite.setTexture(*textures.at(textureAtlasFruitIndex));
		fruitSprite.setOrigin((float) textures.at(textureAtlasFruitIndex)->getSize().x / 2,
										 (float) textures.at(textureAtlasFruitIndex)->getSize().y / 2);

		//Skaliere wenn Feld gr��er als Texture ist
		if(textures.at(textureAtlasFruitIndex)->getSize().x != fieldsize)
		{
			fruitSprite.setScale((float) fieldsize / textures.at(textureAtlasFruitIndex)->getSize().x,
											(float) fieldsize / textures.at(textureAtlasFruitIndex)->getSize().y);
		}
		fruitSprite.setPosition((float)fruitPosition.x * fieldsize + fieldsize / 2,
										   (float)fruitPosition.y * fieldsize + fieldsize / 2);
	}
	void Game::onLevelCompleted()
	{
	}
	void Game::onGameOver()
	{
		sf::Event event;
		while(gameWindow.pollEvent(event))
		{
			switch(event.type)
			{
				case sf::Event::Closed:
					gameWindow.close();
					break;
				case sf::Event::MouseButtonPressed:
				case sf::Event::MouseMoved:
					updateButtons(gameOverButtons);
					break;
				case sf::Event::MouseButtonReleased:
					updateButtons(gameOverButtons, true);
					break;
			}
		}
		renderGameOver();
	}
	void Game::renderGameOver()
	{
		gameWindow.clear(sf::Color::Black);
		gameWindow.draw(backgroundSprite);
		sf::Text text("Spiel verloren!", font);
		text.setPosition((float) gameWindow.getSize().x / 2 - 100, 170);
		for(std::shared_ptr<Button> button : gameOverButtons)
		{
			button->draw();
		}
		gameWindow.draw(text);
		gameWindow.display();
	}
	void Game::loadTextureAtlas()
	{
		int texturesize = 16;
		int rows = 4;
		std::string texturepath = "resources/gfx/textureatlas_c.png";

		int m;
		bool valid = false;
		XMLElement* configElement = configHandle.FirstChildElement("config").FirstChildElement("textures").FirstChildElement("textureatlas").ToElement();
		while(!valid)
		{
			if(configElement)
			{
				configElement->QueryIntAttribute("type", &m);
				if(m == 0 && !christmasmode)
				{
					valid = true;
				}
				else if(m == 1 && christmasmode)
				{
					valid = true;
				}
				else
				{
					configElement = configElement->NextSiblingElement("textureatlas");
				}
			}
		}
		configElement = configElement->FirstChildElement("path");
		if(configElement)
		{
			texturepath = configElement->GetText();
		}
		else
		{
			sl::Logger::writeToLog("textureatlas path is missing or corrupted!");
			texturepath = "resources/gfx/textureatlas.png";
		}
		configElement = configElement->Parent()->FirstChildElement("size");
		if(configElement)
		{
			rows = std::atoi(configElement->GetText());
		}
		else
		{
			sl::Logger::writeToLog("textureatlas size is missing or corrupted!");
			rows = 4;
		}
		configElement->QueryIntAttribute("pixel", &texturesize);
		sf::Image textureAtlas;
		
		sl::Logger::writeToLog(std::string("Texturesize: ") + to_string(texturesize), sl::Logger::LogLevel::extended);
		sl::Logger::writeToLog(std::string("Rows: ") + to_string(rows), sl::Logger::LogLevel::extended);
		//TODO: dynamic path
		if(!christmasmode)
		{
			if(!textureAtlas.loadFromFile(texturepath))
			{
				sl::Logger::writeToLog("Error during loading of the textureatlas!");
				if(!textureAtlas.loadFromFile("resources/gfx/textureatlas.png"))
				{
					sl::Logger::writeToLog("Error during loading of the default textureatlas!");
					fatalerror = true;
				}
			}
		}
		else
		{
			if(!textureAtlas.loadFromFile(texturepath))
			{
				sl::Logger::writeToLog("Error during loading of the christmas textureatlas!");
				if(!textureAtlas.loadFromFile("resources/gfx/textureatlas.png"))
				{
					sl::Logger::writeToLog("Error during loading of the default textureatlas!");
					fatalerror = true;
				}
			}
			christmasHatIndex = 0;
			christmasHeadIndex = 8;
		}
		textures.clear();
		for(int i = 0; i < rows; ++i)
		{
			for(int j = 0; j < rows; ++j)
			{
				sl::Logger::writeToLog("Added Texture", sl::Logger::LogLevel::debug);
				sl::Logger::writeToLog(std::string("Row: ") + to_string(i), sl::Logger::LogLevel::debug);
				sl::Logger::writeToLog(std::string("Cell: ") + to_string(j), sl::Logger::LogLevel::debug);
				textures.push_back(std::shared_ptr<sf::Texture>(new sf::Texture()));
				textures.at(textures.size()-1)->loadFromImage(textureAtlas, sf::IntRect(j*texturesize, i*texturesize, texturesize, texturesize));
			}
		}
	}
	Game::Game()
		: fatalerror(false), configHandle(nullptr), firstSnake(textures, 0), secondSnake(textures, 0)
	{
		sl::Logger::writeToLog("Init randome generator!");
		srand((unsigned int)time(nullptr));
		config.LoadFile("config/gameconfig.xml");
		if(config.Error())
		{
			sl::Logger::writeToLog("Error when loading the config file! Aborting...!");
			sl::Logger::writeToLog("TinyXML2 Errorid: " + std::to_string(config.ErrorID()));
			fatalerror = true;
			return;
		}
		configHandle = &config;
		XMLElement* configElement = configHandle.FirstChildElement("config").FirstChildElement("miscellaneous").FirstChildElement("filelogging").ToElement();
		if(configElement)
		{
			if(std::string(configElement->GetText()) == "true")
			{
				sl::Logger::enabelFileLogging();
			}
		}
		configElement = configHandle.FirstChildElement("config").FirstChildElement("window").FirstChildElement("cells").ToElement();
		if(configElement)
		{
			width = std::atoi(configElement->GetText());
		}
		else
		{
			sl::Logger::writeToLog("Parsingerror at windowcells! Set default Value: 32");
			width = 32;
		}

		configElement = configHandle.FirstChildElement("config").FirstChildElement("window").FirstChildElement("rows").ToElement();
		if(configElement)
		{
			hight = std::atoi(configElement->GetText());
		}
		else
		{
			sl::Logger::writeToLog("Parsingerror at windowrows! Set default Value: 20");
			hight = 20;
		}

		configElement = configHandle.FirstChildElement("config").FirstChildElement("window").FirstChildElement("pixel").ToElement();
		if(configElement)
		{
			fieldsize = std::atoi(configElement->GetText());
		}
		else
		{
			sl::Logger::writeToLog("Parsingerror at windowpixels! Set default Value: 32");
			fieldsize = 32;
		}

		gameWindow.create(sf::VideoMode(width*fieldsize, hight*fieldsize), "Snake", sf::Style::Close | sf::Style::Titlebar);

		std::time_t tz;
		std::time(&tz);
		if(!(tz == (std::time_t)(-1)))
		{
			if(std::localtime(&tz)->tm_mon == 11)
			{
				christmasmode = true;
				sl::Logger::writeToLog("Christmasmode enabled!");
			}
			else
			{
				christmasmode = false;
			}
		}
		else
		{
			sl::Logger::writeToLog("Error during timelookup!");
			christmasmode = false;
		}
		sl::Logger::writeToLog("Load Texture Atlas!");
		loadTextureAtlas();
		firstSnake.setTexturesize(fieldsize);
		secondSnake.setTexturesize(fieldsize);

		std::string background;
		configElement = configHandle.FirstChildElement("config").FirstChildElement("textures").FirstChildElement("backgroundtexture").FirstChildElement("path").ToElement();
		if(configElement)
		{
			background = configElement->GetText();
		}
		else
		{
			sl::Logger::writeToLog("Parsingerror at backgroundtexture! Set default Value: resources/gfx/background.png");
			background = "resources/gfx/background.png";
		}
		if(!backgroundTexture.loadFromFile(background))
		{
			sl::Logger::writeToLog("Error during the loading of the background image!");

		}
		backgroundSprite.setTexture(backgroundTexture);
		backgroundSprite.setPosition(0, 0);
		backgroundSprite.setScale((float)(width*fieldsize) / backgroundTexture.getSize().x, 
								  (float)(hight*fieldsize) / backgroundTexture.getSize().y);
		configElement = configHandle.FirstChildElement("config").FirstChildElement("miscellaneous").FirstChildElement("gamespeed").ToElement();
		if(configElement)
		{
			tickTime = sf::seconds((float) atof(configElement->GetText()));
		}
		else
		{
			sl::Logger::writeToLog("Parsingerror at gamespeed! Set default Value: 0.25f");
			tickTime = sf::seconds(0.25);
		}

		if(!font.loadFromFile("resources/font/Carlito-Regular.ttf"))
		{
			sl::Logger::writeToLog("Error during the loading of the font!");
		}
		//
		//TODO: Replace credit texture -> dynamic text
		//
		creditTexture.loadFromFile("resources/gfx/credits.png");
		float scalefactor = (float) width / creditTexture.getSize().x;
		creditSprite.setTexture(creditTexture);
		creditSprite.setPosition(0, 0);
		creditSprite.setScale(2.0f, 2.0f);

		///////////////
		gamestate = GameState::WELCOME;
		//////////////
		sl::Logger::writeToLog("Creating Buttons...");
		mainMenuButtons.push_back(std::shared_ptr<Button> (new Button(sf::Vector2i(gameWindow.getSize().x / 2 - 100, 150), "resources/gfx/button.png", "resources/gfx/button_H.png", "Spiel starten", "resources/font/Carlito-Regular.ttf", 200, 50, [&]()
		{
			singleplayer = true;
			restartGame();
			gamestate = GameState::PLAYING;
		}, *this)));
		mainMenuButtons.push_back(std::shared_ptr<Button>(new Button(sf::Vector2i(gameWindow.getSize().x / 2 - 100, 210), "resources/gfx/button.png", "resources/gfx/button_H.png", "Multiplayer", "resources/font/Carlito-Regular.ttf", 200, 50, [&]()
		{
			singleplayer = false;
			restartGame();
			gamestate = GameState::PLAYING;
		}, *this)));
		mainMenuButtons.push_back(std::shared_ptr<Button> (new Button(sf::Vector2i(gameWindow.getSize().x / 2 - 100, 270), "resources/gfx/button.png", "resources/gfx/button_H.png", "Optionen", "resources/font/Carlito-Regular.ttf", 200, 50, [&]()
		{
			//gamestate = GameState::OPTIONS;
		}, *this)));
		mainMenuButtons.push_back(std::shared_ptr<Button> (new Button(sf::Vector2i(gameWindow.getSize().x / 2 - 100, 330), "resources/gfx/button.png", "resources/gfx/button_H.png", "Credits", "resources/font/Carlito-Regular.ttf", 200, 50, [&]()
		{
			gamestate = GameState::CREDITS;
		}, *this)));
		mainMenuButtons.push_back(std::shared_ptr<Button> (new Button(sf::Vector2i(gameWindow.getSize().x / 2 - 100, 390), "resources/gfx/button.png", "resources/gfx/button_H.png", "Beenden", "resources/font/Carlito-Regular.ttf", 200, 50, [&]()
		{
			gameWindow.close();
		}, *this)));

		pausingButtons.push_back(std::shared_ptr<Button> (new Button(sf::Vector2i(gameWindow.getSize().x / 2 - 100, 150), "resources/gfx/button.png", "resources/gfx/button_H.png", "Spiel fortsetzen", "resources/font/Carlito-Regular.ttf", 200, 50, [&]()
		{
			gamestate = GameState::PLAYING;
		}, *this)));
		pausingButtons.push_back(std::shared_ptr<Button> (new Button(sf::Vector2i(gameWindow.getSize().x / 2 - 100, 210), "resources/gfx/button.png", "resources/gfx/button_H.png", "Optionen", "resources/font/Carlito-Regular.ttf", 200, 50, [&]()
		{
			//gamestate = GameState::OPTIONS;
		}, *this)));
		pausingButtons.push_back(std::shared_ptr<Button> (new Button(sf::Vector2i(gameWindow.getSize().x / 2 - 100, 270), "resources/gfx/button.png", "resources/gfx/button_H.png", "Beenden", "resources/font/Carlito-Regular.ttf", 200, 50, [&]()
		{
			gamestate = GameState::WELCOME;
		}, *this)));

		gameOverButtons.push_back(std::shared_ptr<Button>(new Button(sf::Vector2i(gameWindow.getSize().x / 2 - 100, 210), "resources/gfx/button.png", "resources/gfx/button_H.png", "Spiel neustarten!", "resources/font/Carlito-Regular.ttf", 200, 50, [&]()
		{
			gamestate = GameState::PLAYING;
			restartGame();
		}, *this)));
		gameOverButtons.push_back(std::shared_ptr<Button>(new Button(sf::Vector2i(gameWindow.getSize().x / 2 - 100, 270), "resources/gfx/button.png", "resources/gfx/button_H.png", "Zur�ck zum Hauptmen�", "resources/font/Carlito-Regular.ttf", 200, 50, [&]()
		{
			gamestate = GameState::WELCOME;
		}, *this)));

		creditsToMainButton = std::shared_ptr<Button>(new Button(sf::Vector2i(gameWindow.getSize().x / 2 - 100, gameWindow.getSize().y - 100), "resources/gfx/button.png", "resources/gfx/button_H.png", "Zur�ck zum Hauptmen�", "resources/font/Carlito-Regular.ttf", 200, 50, [&]()
		{
			gamestate = GameState::WELCOME;
		}, *this));
		sl::Logger::writeToLog("Finished creating of Buttons!");
		sl::Logger::writeToLog("Restart gameclock!");
		gameClock.restart();
		sl::Logger::writeToLog("GameClock restarted!");
	}
	void Game::restartGame()
	{
		christmasHatGenerated = false;
		sl::Logger::writeToLog("Restart Game!");
		sl::Logger::writeToLog("Reload all gamesprites!", sl::Logger::LogLevel::extended);
		headindex = 1;
		if(singleplayer)
			firstSnake.reset(std::vector<sf::Vector2i> {sf::Vector2i(width / 2 - 2, hight / 2),
														sf::Vector2i(width / 2 - 1, hight / 2),
														sf::Vector2i(width / 2, hight / 2),
														sf::Vector2i(width / 2 + 1, hight / 2)}, false);
		else
		{
			firstSnake.reset(std::vector<sf::Vector2i> {sf::Vector2i(width / 2 - 2, hight / 2 + 2),
							 sf::Vector2i(width / 2 - 1, hight / 2 + 2),
							 sf::Vector2i(width / 2, hight / 2 + 2),
							 sf::Vector2i(width / 2 + 1, hight / 2 + 2)}, false);
			secondSnake.reset(std::vector<sf::Vector2i> {sf::Vector2i(width / 2 - 2, hight / 2 - 2),
							 sf::Vector2i(width / 2 - 1, hight / 2 - 2),
							 sf::Vector2i(width / 2, hight / 2 - 2),
							 sf::Vector2i(width / 2 + 1, hight / 2 - 2)}, true);
		}
		generateNewFruit();
		firstPlayerDirection = Direction::LEFT;
		secondPlayerDirection = Direction::LEFT;
	}
	void Game::loop()
	{
		while(gameWindow.isOpen() && !fatalerror)
		{
			switch(gamestate)
			{
			case GameState::OPTIONS:
				onOptions();
				break;
			case GameState::CREDITS:
				onCredits();
				break;
			case GameState::WELCOME:
				onWelcome();
				break;
			case GameState::PAUSING:
				onPausing();
				break;
			case GameState::PLAYING:
				onPlaying();
				break;
			case GameState::PLAYERWON:
			case GameState::LEVELCOMPLETED:
				onLevelCompleted();
				break;
			case GameState::GAMEOVER:
				onGameOver();
				break;
			}
		}
	}

	void Game::updateButtons(std::vector<std::shared_ptr<Button>>& buttons, bool buttonreleased)
	{
		sf::Time newTime = gameClock.getElapsedTime();
		for(std::shared_ptr<Button> button : buttons)
		{
			button->update(buttonreleased);
		}
		elapsedTime = newTime;
	}
}
