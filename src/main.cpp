#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "../include/Game.hpp"
#include <Logger.hpp>


int main(int argc, char** argv)
{
	if(argc > 1)
	{
		if(argv[1][0] == '-' || argv[1][0] == '/')
		{
			if(argv[1][1] == 'd')
			{
				sl::Logger::setGlobalLogLevel(sl::Logger::LogLevel::debug);
				sl::Logger::enabelFileLogging();
			}
		}
	}
	sl::Logger::writeToLog("Starting Snake...");
	snake::Game game;
	game.loop();
}