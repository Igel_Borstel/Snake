/*
GNOP
----

Copyright (c) 2015 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
#include "../include/button.hpp"
#include "../include/Game.hpp"
#include <iostream>
#include <cstdlib>

#ifdef _WIN32
#include <Windows.h>
#endif //_WIN32

namespace snake
{
	Button::Button(sf::Vector2i pos, std::string normal, std::string active, std::string hover, std::string text, std::string font, int w, int h, std::function<void()> cb, Game& g)
		: pos1(pos), game(g), width(w), hight(h), callback(cb)
	{
		//Laden der Resourcen (Texturen; Fonts
		sf::Image image;
		image.create(width, hight, sf::Color::Blue);
		if(!normalTexture.loadFromFile(normal))
		{
			normalTexture.loadFromImage(image);
		}
		if(!activeTexture.loadFromFile(active))
		{
			activeTexture.loadFromImage(image);
		}
		if(!hoverTexture.loadFromFile(hover))
		{
			hoverTexture.loadFromImage(image);
		}

		if(buttonFont.loadFromFile(font))
		{
			buttonText.setFont(buttonFont);
			buttonText.setColor(sf::Color::Black);
		}
		else
		{
		#ifdef _WIN32 //Wenn Resourceloading fehlgeschlagen -> Default Windows Resourcen laden
			const int BUFFERSIZE = 30;
			TCHAR envvar[BUFFERSIZE] = " ";
			GetEnvironmentVariable("windir", envvar, sizeof(envvar));
		
			if(envvar != nullptr && envvar[0] != ' ')
			{
				std::string envpath;
				envpath = envvar;
				if(!envpath.empty())
				{
					buttonFont.loadFromFile(envpath + std::string("/fonts/arial.ttf"));
					buttonText.setFont(buttonFont);
					buttonText.setColor(sf::Color::White);
				}
			}
		#endif //_WIN32
		}
		buttonText.setString(text);

		if(!(normalTexture.getSize() == activeTexture.getSize() && normalTexture.getSize() == hoverTexture.getSize()))
		{
		}

		pos2.x = pos1.x + width;
		pos2.y = pos1.y + hight;

		//Buttonsprite und Text konstruieren
		sprite.setPosition((float)pos1.x, (float)pos1.y);
		sprite.setTexture(normalTexture);
		sprite.setScale((float)width / normalTexture.getSize().x, (float)hight / normalTexture.getSize().y);

		buttonText.setPosition((float)pos1.x, (float)pos1.y);
		buttonText.setCharacterSize(45);
		while((buttonText.getLocalBounds().width > width) || (buttonText.getLocalBounds().height > hight))
		{
			buttonText.setCharacterSize((buttonText.getCharacterSize() - 1));
		}
	}

	Button::Button(sf::Vector2i pos, std::string normal, std::string text, std::string font, int w, int h, std::function<void()> cb, Game& g)
		: Button(pos, normal, normal, normal, text, font, w, h, cb, g)
	{
	}

	Button::Button(sf::Vector2i pos, std::string normal, std::string hover, std::string text, std::string font, int w, int h, std::function<void()> cb, Game& g)
		: Button(pos, normal, normal, hover, text, font, w, h, cb, g)
	{

	}

	Button::~Button()
	{
	}

	bool Button::isOnButton(sf::Vector2i pos)
	{
		return ((pos.x >= pos1.x && pos.x <= pos2.x) && (pos.y >= pos1.y && pos.y <= pos2.y)) ? true : false;
	}

	void Button::setActive()
	{
		sprite.setTexture(activeTexture);
	}

	void Button::setHover()
	{
		sprite.setTexture(hoverTexture);
	}

	void Button::setNormal()
	{
		sprite.setTexture(normalTexture);
	}

	void Button::draw()
	{
		game.getWindow().draw(sprite);
		game.getWindow().draw(buttonText);
	}

	void Button::update(bool buttonreleased)
	{
		if(isOnButton(sf::Mouse::getPosition(game.getWindow())))
		{
			if(buttonreleased)
			{
				setActive();
				//callback aufrufen
				callback();
			}
			else
			{
				setHover();
			}
		}
		else
		{
			setNormal();
		}
	}
}