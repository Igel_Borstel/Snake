#ifndef GAME_HPP
#define GAME_HPP

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <vector>
#include <memory>
#include <tinyxml2.h>
#include "button.hpp"
#include "Snake.hpp"
#include "Direction.hpp"

namespace snake
{
	class Snake;

	class Game
	{
	private:

		enum class GameState
		{
			OPTIONS,
			CREDITS,
			WELCOME,
			PAUSING,
			PLAYING,
			LEVELCOMPLETED,
			GAMEOVER,
			PLAYERWON
		} gamestate;
		
		Direction firstPlayerDirection;
		Direction secondPlayerDirection;

		int width, hight;
		int fieldsize;
		bool christmasmode;

		sf::Vector2i fruitPosition;
		//std::vector<sf::Vector2i> snakeBody;

		sf::Texture backgroundTexture;
		sf::Sprite backgroundSprite;
		sf::Texture creditTexture;
		sf::Sprite creditSprite;


		int headindex;
		int textureAtlasFruitIndex;
		int christmasHeadIndex;
		int christmasHatIndex;
		bool christmasHatGenerated;
	public:
		std::vector<std::shared_ptr<sf::Texture>> textures;
	private:
		sf::Sprite fruitSprite;
		sf::Font font;
	public:
		sf::RenderWindow gameWindow;
	private:
		sf::Clock gameClock;
		sf::Time tickTime;
		sf::Time elapsedTime;
		
		std::vector<std::shared_ptr<Button>> mainMenuButtons;
		std::vector<std::shared_ptr<Button>> pausingButtons;
		std::vector<std::shared_ptr<Button>> gameOverButtons;
		std::shared_ptr<Button> creditsToMainButton;

		tinyxml2::XMLDocument config;
		tinyxml2::XMLHandle configHandle;
		bool fatalerror;

		bool singleplayer;
		Snake firstSnake;
		Snake secondSnake;

		void onOptions();
		void onCredits();
		void onWelcome();
		void renderWelcome();
		void onPausing();
		void renderPausing();

		void onPlaying();
		void updateGame();
		void renderGame();
		void generateNewFruit();
		void restartGame();

		void onLevelCompleted();
		void onGameOver();
		void renderGameOver();

		void updateButtons(std::vector<std::shared_ptr<Button>>&, bool = false);

		void loadTextureAtlas();
	public:
		Game();
		void loop();
		sf::RenderWindow& getWindow();
		sf::Vector2i getFruitPosition() const;
	};

	inline sf::RenderWindow& Game::getWindow()
	{
		return gameWindow;
	}
	inline sf::Vector2i Game::getFruitPosition() const
	{
		return fruitPosition;
	}
}

#endif //GAME_HPP