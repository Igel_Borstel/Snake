/*
GNOP
----

Copyright (c) 2015 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
#ifndef BUTTON_HPP
#define BUTTON_HPP


#include <SFML/Graphics.hpp>
#include <string>
#include <functional>



namespace snake
{
	class Game;

	class Button
	{
	private:
		sf::Vector2i pos1;
		sf::Vector2i pos2;
		int width;
		int hight;

		sf::Texture normalTexture;
		sf::Texture activeTexture;
		sf::Texture hoverTexture;
		sf::Text buttonText;
		sf::Font buttonFont;

		sf::Sprite sprite;
		
		Game& game;

		void setActive();
		void setHover();
		void setNormal();

		bool isOnButton(sf::Vector2i);

		//Functor zur Callbackfunktion
		std::function<void()> callback;
	public:
		Button(sf::Vector2i, std::string, std::string, std::string, std::string, std::string, int, int, std::function<void()>, Game&);
		Button(sf::Vector2i, std::string, std::string, std::string, std::string, int, int, std::function<void()>, Game&);
		Button(sf::Vector2i, std::string, std::string, std::string, int, int, std::function<void()>, Game&);
		~Button();
		
		void draw();
		void update(bool);
	};
}


#endif //BUTTON_HPP