#ifndef SNAKE_HPP
#define SNAKE_HPP

#include <vector>
#include <SFML/Graphics.hpp>
#include <memory>
//#include "Game.hpp"
#include "Direction.hpp"

namespace snake
{
	class Game;
	
	class Snake
	{
		std::vector<sf::Vector2i> snakeBody;
		std::vector<std::shared_ptr<sf::Texture>>& textures;
		std::vector<sf::Sprite> sprites;
		sf::Vector2i oldSnakeHeadPosition;
		sf::Vector2i oldSnakeTailPosition;
		int fieldsize;
	public:
		Snake(std::vector<std::shared_ptr<sf::Texture>>&, std::vector<sf::Vector2i>, int, bool);
		Snake(std::vector<std::shared_ptr<sf::Texture>>&, int);
		void move(Direction);
		bool collides(Snake&);
		bool collides(int, int);
		bool collides(sf::Vector2i, bool = false);
		void grow(bool);
		void reset(std::vector<sf::Vector2i>, bool);
		void draw(sf::RenderWindow&);
		void setTexturesize(int);
	};
	inline void Snake::setTexturesize(int ts)
	{
		fieldsize = ts;
	}
}

#endif //SNAKE_HPP