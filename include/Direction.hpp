#ifndef DIRECTION_HPP
#define DIRECTION_HPP

namespace snake
{
	enum class Direction
	{
		LEFT,
		RIGHT,
		FORWARD,
		BACKWARD
	};
}

#endif //DIRECTION_HPP